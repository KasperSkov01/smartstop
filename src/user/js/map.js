var markersArray = []
console.log("Working")
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: {lat: 55.329078, lng: 10.235296},
    disableDefaultUI: true,
    zoomControl: true
  });
  var geocoder = new google.maps.Geocoder();

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(addMarker, showError);
  } else { 
    console.log("Geolocation is not supported by this browser.");
  }

  document.getElementById('submit').addEventListener('click', function() {
    map.zoom = 13;
    geocodeAddress(geocoder, map);
  });
}

function showError(error) {
  switch(error.code) {
    case error.PERMISSION_DENIED:
    console.log("User denied the request for Geolocation.")
    break;
    case error.POSITION_UNAVAILABLE:
    console.log("Location information is unavailable.")
    break;
    case error.TIMEOUT:
    console.log("The request to get user location timed out.")
    break;
    case error.UNKNOWN_ERROR:
    console.log("An unknown error occurred.")
    break;
  }
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === 'OK') {
      clearOverlays();
      resultsMap.setCenter(results[0].geometry.location);
      // var marker = new google.maps.Marker({
      //   map: resultsMap,
      //   position: results[0].geometry.location
      // });
      addMarker(results[0].geometry.location);
      markersArray.push(marker)

      document.getElementById("longtitude").value = results[0].geometry.location.lng();
      document.getElementById("latitude").value = results[0].geometry.location.lat();

      update();

    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function addMarker(position) {
  console.log("Add marker");
  var marker = new google.maps.Marker({
        map: fsdfsdfsdf,
        position: position
});
}

function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
  markersArray.length = 0;
}