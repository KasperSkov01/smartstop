<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SmartStop API-demonstration</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<!-- Include style sheets-->
	<?php include 'css/style.html'; ?>
</head>
<body>
	<!-- Include javascript files -->
	<?php  include 'js/script.html'; ?>

	<!-- Map -->
    <div id="floating-panel">
      <input id="address" type="textbox" value="Odense, Danmark">
      <input id="submit" type="button" value="Søg">
    </div>
    <div id="map"></div>

	<!-- Define inputs and layout -->
	<div style="text-align: center; margin-top: 30px">
		<h2>Find your nearest bus stop</h2>
		<p>Edit geocoordinates and see the distance and the neares bus stop change. Maximum 6 digits.</p>
		<p style="font-weight: bold;">Type latitude:</p>
		<input id="latitude" style="width: 150px" type="number" min="50" max="60" step="0.001" value="55.378669" placeholder="55.378669" onkeyup="update()" onchange="update()">

		<p style="font-weight: bold;">Type longtitude:</p>
		<input id="longtitude" style="width: 150px" type="number" min="5" max="15" step="0.001" value="10.371568" placeholder="10.371568" onkeyup="update()" onchange="update()"> 		
		<p id="stopName"></p>
		<p id="distance"></p>
		<table id="routesTable" style="width:100%">
			<thead>
				<th style="width: 10%">Checked</th>
				<th style="width: 25%">Bus</th>
				<th style="width: 40%">Destination</th> 
				<th style="width: 25%">Departure time</th>
			</thead>
			<tbody id="tableBody"></tbody>
		</table>
		<p id="routes"></p>

		<input type="checkbox" id="checkbox1"> Show all JSON
		<br>
		<p id="allJSON" hidden="true"></p>
	</div>

	<!-- Javascript -->
	<script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/map.js"></script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtraM5l844HWGzGx2rFx-YPCsIV54lXN4&callback=initMap">
    </script>
</body>
</html>