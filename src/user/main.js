// Toggle all JSON paragraph visibility on checkbox toggle
$('#checkbox1').mousedown(function() {
	if (!this.checked) {
		this.checked;
		document.getElementById("allJSON").hidden = false;
		$(this).trigger("change");
	} else {
		document.getElementById("allJSON").hidden = true;
	}
});
// When the search button is pressed / text edited
function update() {
	// Get input values and trim to 6 decimals
	var latitude = document.getElementById("latitude").value;
	longtitude = Math.floor(longtitude * 1000000) / 1000000 * 1000000;

	var longtitude = document.getElementById("longtitude").value;
	longtitude = Math.floor(longtitude * 1000000) / 1000000 * 1000000;

	// Make the request for the nearest location from geo-coordinates to the server-side script
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Parse the successful response to JSON
			var obj = JSON.parse(this.responseText);
        	// Display the result
        	// document.getElementById("allJSON").innerHTML = this.responseText;
        	// document.getElementById("stopName").innerHTML = "Stop name: " + obj.LocationList.StopLocation.name;
        	// document.getElementById("distance").innerHTML = "Distance: " + obj.LocationList.StopLocation.distance + "m";

        	// Load the routes
        	loadRoutes(obj.LocationList.StopLocation.name, obj.LocationList.StopLocation.id);
            }
    };
    // xmlhttp.open("GET", "php/api-call.php?type=" + "nearestStop" + "&latitude=" + latitude + "&longtitude=" + longtitude, true);
    xmlhttp.open("GET", "php/api-call.php?type=" + "nearestStop" + "&latitude=10371564&longtitude=55378662", true);

    xmlhttp.send();
};

    function loadRoutes(stopName, stopId) {
    	// Get time and date
    	var date = new Date();
    	var dd = date.getDate();
		var mm = date.getMonth()+1; //January is 0!
		var yyyy = date.getFullYear();

		if(dd<10) {
			dd = '0'+dd;
		} 

		if(mm<10) {
			mm = '0'+mm;
		} 
		var yy = yyyy.toString().slice(2,4);
		date = dd + '.' + mm + '.' + yy;
		var time = new Date();
		var hour = time.getHours();
		var minute = time.getMinutes();
		if (hour < 10) {
			hour = "0"+hour;
		}
		if (minute < 10) {
			minute = "0"+minute;
		}
		time = hour + ":" + minute;

		time = "07:20"
		date = "07.01.19"
		window.alert(time)
		console.log(date);
		console.log(time);

	// Make the request to the server side script
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Parse the successful response to JSON
			var obj = JSON.parse(this.responseText);
			// Load table
           	var table = document.getElementById("tableBody");
           	// Clear the old rows
			$("#tableBody tr").remove(); 
			// List of departures from the API
			var departures = obj.DepartureBoard.Departure;

			// Fill the table with departures
			departures.forEach(function(departure) {
				var busName = departure.name.replace("Bus ","");
				var busDest = departure.direction;
				var busDepartureTime = departure.time;
				var row = table.insertRow(-1);
				var checked = row.insertCell(0);
				var bus = row.insertCell(1);
				var direction = row.insertCell(2);
				var time = row.insertCell(3);

				checked.innerHTML = "";
				bus.innerHTML = busName;
				direction.innerHTML = busDest;
				time.innerHTML = busDepartureTime;
			});
		}
	};
	xmlhttp.open("GET", "php/api-call.php?type=" + "routes" + "&stopId=" + stopId + "&date=" + date + "&time=" + time, true);
	xmlhttp.send();
}
    window.alert("Going to request.")

// Load the data for the first time
update();
// Check if input is a number
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}