<?php 
ob_start();
require 'php/db.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="user-scalable=no" />
	<title>Device - SmartStop</title>
	<!-- Include style sheets-->
	<?php include 'css/style.html'; ?>
</head>
<!-- Start the clock when the screen has loaded -->
<body onload="clock()">
	<!-- Include javascript files -->
	<?php  include 'js/script.html'; ?>
	<!-- Define inputs and layout -->

	<!-- Popup -->
	<div id="verifyDiv" class="verify" style="display: none;">
		<div class="container-fluid">
			<h1 style="color: white;">Er du sikker?</h1>
			<h3 id="verifyParagraph" style="color: white;">NR - BUS NAVN | TID</h3>
			<div class="center">
				<img class="verifyButton yes" src="assets/yes-icon.png" onclick="yes()">
				<img class="verifyButton no" src="assets/no-icon.png" onclick="no()">
			</div>
		</div>
	</div>
	<div id="allDiv">
		<!-- routesTable header -->
		<div>
			<table id="headerTable">
				<thead>
					<th class="headerChecked" style=""><p class="header-p">Valgt</p></th>
					<th class="headerBus"><p class="header-p">Bus</p></th>
					<th class="headerDirection"><p class="header-p">Destination</p></th> 
					<th class="headerTime"><p class="header-p">Tidspunkt</p></th>
					<th><p id="clock" class="header-p" onClick="window.location.reload()">00:00</p></th>
				</thead>
			</table>
		</div>
		<!-- Table containing the routes -->
		<div id="routesContainer">

			<table id="routesTable">
				<tbody id="tableBody"></tbody>
			</table>
			<br>
			<br>
		</div>
		<!-- Debug table -->
		<div style="display: none;">
			<table>
				<tbody>
					<tr>
						<td><p id="stopName"></p></td>
						<td width="5%"></td>
						<td><p id="distance"></p></td>
						<td width="5%"></td>
						<td><p id="messageField">Messages:</p></td>
						<input type="button" value="Refresh Page" onClick="location.href=location.href">
					</tr>
				</tbody>
			</table>
		</div>
		<div class="center">
			<select onchange="select(this.value);">
				<option value="sde">SDE Munkebjergvej (Odense Kommune)</option>
				<option value="odense-st">Odense St.</option>
				<option value="tarupcenter">Tarup Center (Odense Kommune)</option>
				<option value="morud">Morud Centret (Nordfyns Kommune)</option>
				<option value="nyborg">Lindealleen (Nyborg Kommune)</option>
				<option value="middelfart">Adlerhusvej (Middelfart Kommune)</option>
			</select>
		</div>
	</div>
	<!-- Javascript -->
	<script type="text/javascript" src="js/clock.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>