//move to main.js for live GPS-Support
function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else { 
		console.log("Geolocation is not supported by this browser.");
		messageField.innerHTML += "<br><br>Geolocation is not supported by this browser.";
		messageField.style.display = "block";
	}
}

function showPosition(position) {
	messageField.innerHTML += "<br><br>Geolocation is supported by this browser.";
	console.log("User lat: " + position.coords.latitude + 
		" Long: " + position.coords.longitude);
	Load the data for the first time
	update(position.coords.latitude, position.coords.longitude);
}
function showError(error) {
	messageField.innerHTML += "<br><br>Geolocation error.";
	messageField.style.display = "block";
	switch(error.code) {
		case error.PERMISSION_DENIED:
		messageField.innerHTML += "<br><br>User denied the request for Geolocation.";
		break;
		case error.POSITION_UNAVAILABLE:
		messageField.innerHTML += "<br><br>Location information is unavailable.";
		break;
		case error.TIMEOUT:
		messageField.innerHTML += "<br><br>The request to get user location timed out.";
		break;
		case error.UNKNOWN_ERROR:
		messageField.innerHTML += "<br><br>An unknown error occurred.";
		break;
	}
}