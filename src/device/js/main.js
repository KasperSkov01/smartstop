messageField = document.getElementById("messageField");
stopName = document.getElementById("stopName");
distance = document.getElementById("distance");
var stop_id, stop_name, routes, date, rowIndex, cellToCheck;
i = 0;
maxRoutes = 8 - 1; 
selected = "sde";
addedRoutes = [];

function select(name) {
	switch (name) {
		case "odense-st":
		selected = "odense-st";
		update(55.401534, 10.386822);
		break;
		case "sde":
		selected = "sde";
		update(55.380976, 10.408357);
		break;
		case "tarupcenter":
		selected = "tarupcenter";
		update(55.408065, 10.339877);
		break;
		case "morud":
		selected = "morud";
		update(55.444342, 10.188910);
		break;
		case "nyborg":
		selected = "nyborg";
		update(55.314772, 10.791249);
		break;
		case "middelfart":
		selected = "middelfart";
		console.log("middelfart")
		update(55.501873, 9.734020);
		break;
	}
}

function update(lat, lng) {
	// Trim coordinates to 6 decimals
	var latitude = Math.floor(lat * 1000000) / 1000000 * 1000000;
	var longtitude = Math.floor(lng * 1000000) / 1000000 * 1000000;
	// Make the request for the nearest bus stop from geo-coordinates to the server-side script
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Parse the successful response to JSON
			var obj = JSON.parse(this.responseText);
        	// Display the result
        	stopName.innerHTML = "Stop name: " + obj.LocationList.StopLocation.name;
        	distance.innerHTML = "Distance: " + obj.LocationList.StopLocation.distance + "m";
        	stop_name = obj.LocationList.StopLocation.name;
        	stop_id = obj.LocationList.StopLocation.id;
        	// Load the routes
        	loadRoutes(obj.LocationList.StopLocation.name, obj.LocationList.StopLocation.id);
        }
    };
    xmlhttp.open("GET", "php/api-call.php?type=" + "nearestStop" + "&latitude=" + latitude + "&longtitude=" + longtitude, true);
    xmlhttp.send();
};

function loadRoutes(stop_name, busStopId) {
	console.log(stop_name + " Id: " + busStopId)
	// Get time and date
	var timeAndDate = timeDate()
	var time = timeAndDate[0];
	date = timeAndDate[1];
	console.log("Time: " + time + " Date: " + date);

	// Make the request to the server side script
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Parse the successful response to JSON
			var obj = JSON.parse(this.responseText);
			console.log("OBJECT:")
						console.log("OBJECT:")

			console.log("OBJECT:")
			document.getElementById("consoleP").innerHTML = obj;
			console.log(obj);
			// Load table
			var table = document.getElementById("tableBody");
           	// Clear the old rows
           	$("#tableBody tr").remove(); 
           	if (typeof(obj.DepartureBoard.error) != "undefined") {
           		messageField.style.display = "block";
           		messageField.innerHTML += obj.DepartureBoard.error;
           		return;
           	} else {
           		messageField.style.display = "block";
           		messageField.innerHTML += "<br><br>No error from Rejseplanen";
           	}
			routes = obj.DepartureBoard.Departure;
			// Ask Rejseplanen for routes checked with this stop_id and update the table
			tableUpdate(routes, table);
		}
	};
	xmlhttp.open("GET", "php/api-call.php?type=" + "routes" + "&stopId=" + busStopId + "&date=" + date + "&time=" + time, true);
	xmlhttp.send();
}

$(document).ready(function(){
	$("div#routesContainer").delegate('table', 'click', function(e) {
		console.log("Click");
		var row = e.target.closest("tr");
		if (!row.classList.contains("routeRow")) {
			return;
		}
		rowIndex = getChildNumber(row);
		element = row.childNodes[0];
		departure = routes[addedRoutes[rowIndex] - 1];
		bus = departure.name.replace("Bus ","");
		dest = departure.direction;
		dep_date = departure.date;
		dep_time = departure.time;
		document.getElementById("verifyParagraph").innerHTML = bus + " - " + dest + " | " + dep_time;
		verify();
	});
});

function getChildNumber(node) {
	return Array.prototype.indexOf.call(node.parentNode.childNodes, node);
}

function dbSave(rowIndex) {
	console.log(rowIndex);
	console.log(addedRoutes[rowIndex]);
	console.log(routes[addedRoutes[rowIndex] - 1])
	departure = routes[addedRoutes[rowIndex] - 1];
	bus = departure.name.replace("Bus ","");
	dest = departure.direction;
	dep_date = departure.date;
	dep_time = departure.time;
	console.log(stop_name);
	console.log("Bus: " + bus);
	console.log("Dest: " + dest);
	console.log("dep_date: " + dep_date);
	console.log("dep_time: " + dep_time);
	console.log("stop_id" + stop_id);

	$.post("php/server_save_check.php", {bus: bus, dest: dest, dep_date: dep_date, dep_time: dep_time, stop_id: stop_id}, function(result){
		console.log(result)
		messageField.innerHTML += result;
	});
}

function tableUpdate(routes, table) {
	$.post("php/server_get_checks.php", {stop_id: stop_id}, function(result){
		// Fill the table with departures
			addedRoutes = [];
		routes.forEach(function(departure) {
				
				if (addedRoutes > maxRoutes) {
					return;
				}
				// Make sure, that there is no duplicate appartures
				if (i > 0) {
					if (departure.name.replace("Bus ","") == routes[i - 1].name.replace("Bus ","")) {
						if (departure.direction == routes[i - 1].direction) {
							console.log(i);
							console.log("Similar bus, " + departure.name.replace("Bus ","") + " " + routes[i - 1].name.replace("Bus ",""));
							console.log("return");
							i++;
							return;
						}
					}
				}

				var row = table.insertRow(-1);
				row.classList.add("routeRow");
				// var spacing = row.insertCell(0);
				var checkedCell = row.insertCell(0);
				var busCell = row.insertCell(1);
				var directionCell = row.insertCell(2);
				var timeCell = row.insertCell(3);
				// spacing.classList.add("cell");
				checkedCell.classList.add("cell", "checkedCell");
				busCell.classList.add("cell", "busCell");
				directionCell.classList.add("cell", "directionCell");
				timeCell.classList.add("cell", "timeCell");

				checkedCell.style.textAlign = "left";
				busCell.style.textAlign = "left";
				directionCell.style.textAlign = "left";
				timeCell.style.textAlign = "left";

				checkedCell.style.width = "12%";
				busCell.style.width = "16%";
				directionCell.style.width = "42%";
				timeCell.style.width = "30%";

				checkedCell.innerHTML = '';


				var cell_dep_name = departure.name.replace("Bus ","");
				busCell.innerHTML = cell_dep_name;
				directionCell.innerHTML = departure.direction;
				timeCell.innerHTML = departure.time;

				
				
				// console.log(result)
				if (result == "none") {
					messageField.innerHTML += "<br><br>No previously checked routes.";
				} else if (result == "error") {
					messageField.innerHTML += "<br><br>An error occured.";
				} else {
					checkedRoutes = JSON.parse(result);
					checkedRoutes.forEach(function(checkedRoute) {
						if (checkedRoute.bus == cell_dep_name) {
							if (checkedRoute.dep_time == departure.time) {
								if (checkedRoute.dest == departure.direction) {
									checkedCell.innerHTML = '<img class="checked" src="assets/check-icon.png" alt="Checked">';
								}
							}
						}
					})
				}
				i++;
				addedRoutes.push(i);
				console.log(addedRoutes);
			});
			i = 0;
			
		messageField.innerHTML += "<br><br>Successfully loaded checked routes from the database.";
	});
}

function verify() {
	document.getElementById("verifyDiv").style.display = "block";
	document.getElementById("allDiv").style.display = "none";
}

function yes() {
	dbSave(rowIndex);
	element.innerHTML = '<img class="checked" src="assets/check-icon.png" alt="Checked">';
	document.getElementById("verifyDiv").style.display = "none";
	document.getElementById("allDiv").style.display = "block";
}

function no() {
	document.getElementById("verifyDiv").style.display = "none";
	document.getElementById("allDiv").style.display = "block";
}

// Time and date
function timeDate() {
	var date = new Date();
	var dd = date.getDate();
		var mm = date.getMonth()+1; //January is 0!
		var yyyy = date.getFullYear();
		if(dd<10) {
			dd = '0'+dd;
		} 
		if(mm<10) {
			mm = '0'+mm;
		} 
		var yy = yyyy.toString().slice(2,4);
		date = dd + '.' + mm + '.' + yy;
		var time = new Date();
		var hour = time.getHours();
		var minute = time.getMinutes();
		if (hour < 10) {
			hour = "0"+hour;
		}
		if (minute < 10) {
			minute = "0"+minute;
		}
		time = hour + ":" + minute;
		return [time, date];
		// Fixated time:
		// return ["14:15","23.01.19"];
	}